package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

const (
	Reset  = "\033[0m"
	Red    = "\033[31m"
	Green  = "\033[32m"
	Yellow = "\033[33m"
	Purple = "\033[35m"
	Blue   = "\033[34m"
)

var stop atomic.Bool

type Philosophers struct {
	size        int
	timeToDie   int64
	timeToEat   int64
	timeToSleep int64
	startTime   int64
	mutexes     []sync.Mutex
	diedPhilo   uint32
	deathTime   int64
	mtx         sync.Mutex
	shared      []bool
}

type Philosopher struct {
	id      uint32
	lastAte int64
	philos  *Philosophers
}

func main() {
	if len(os.Args) != 5 {
		log.Fatal("Invalid arguments")
	}
	philos := Philosophers{}
	if err := Parser(&philos, os.Args); err != nil {
		log.Fatal(err)
	}

	philos.mutexes = make([]sync.Mutex, philos.size)
	philos.shared = make([]bool, philos.size)
	philos.startTime = time.Now().UnixMilli()
	execProgram(&philos)
}

func execProgram(global *Philosophers) {
	philos := make([]Philosopher, global.size)
	wg := sync.WaitGroup{}

	startSim := func(philo *Philosopher) {
		if philo.id%2 == 1 && philo.philos.size > 1 {
			customSleep(philo, global.timeToSleep/2)
		}
		for {
			if stop.Load() || takeFork(philo, philo.id-1) || takeFork(philo, (philo.id)%uint32(philo.philos.size)) ||
				eat(philo) || releaseForks(philo) || goToSleep(philo) {
				wg.Done()
				return
			}
			printMsg(Purple, philo.id, time.Now().UnixMilli()-global.startTime, "is thinking", Reset)
		}
	}

	for i := 0; i < global.size; i++ {
		wg.Add(1)
		philos[i].id = uint32(i + 1)
		philos[i].lastAte = global.startTime
		philos[i].philos = global
		go startSim(&philos[i])
	}

	wg.Wait()
	fmt.Printf("%s[%v] %d died\n%s", Red, global.deathTime, global.diedPhilo, Reset)
}

func takeFork(philo *Philosopher, index uint32) bool {
	philo.philos.mutexes[index].Lock()

	if philo.philos.shared[index] {
		philo.philos.mutexes[index].Unlock()
		for {
			if checkAndUpdateStatus(time.Now().UnixMilli(), philo) {
				return true
			}
			time.Sleep(time.Microsecond * 200)
			philo.philos.mutexes[index].Lock()
			if !philo.philos.shared[index] {
				break
			}
			philo.philos.mutexes[index].Unlock()
		}
	}
	philo.philos.shared[index] = true
	printMsg(Blue, philo.id, time.Now().UnixMilli()-philo.philos.startTime, "picked up a fork", Reset)
	philo.philos.mutexes[index].Unlock()
	return false
}

func eat(philo *Philosopher) bool {
	printMsg(Green, philo.id, time.Now().UnixMilli()-philo.philos.startTime, "is eating", Reset)
	philo.lastAte = time.Now().UnixMilli()
	return customSleep(philo, philo.philos.timeToEat)
}

func goToSleep(philo *Philosopher) bool {
	printMsg(Yellow, philo.id, time.Now().UnixMilli()-philo.philos.startTime, "is sleeping", Reset)
	return customSleep(philo, philo.philos.timeToSleep)
}

// releases forks after eating
func releaseForks(philo *Philosopher) bool {
	philo.philos.mutexes[philo.id-1].Lock()
	philo.philos.shared[philo.id-1] = false
	philo.philos.mutexes[philo.id-1].Unlock()

	philo.philos.mutexes[philo.id%uint32(philo.philos.size)].Lock()
	philo.philos.shared[philo.id%uint32(philo.philos.size)] = false
	philo.philos.mutexes[philo.id%uint32(philo.philos.size)].Unlock()
	return false
}

// custom sleep function to sleep in increments
func customSleep(philo *Philosopher, timeToSleep int64) bool {
	start := time.Now().UnixMilli()

	for {
		now := time.Now().UnixMilli()
		if checkAndUpdateStatus(now, philo) {
			return true
		}
		if now-start >= timeToSleep {
			break
		}
		time.Sleep(time.Microsecond * 200)
	}
	return false
}

func checkAndUpdateStatus(now int64, philo *Philosopher) bool {
	if now-philo.lastAte >= philo.philos.timeToDie {
		if !stop.Load() {
			philo.philos.mtx.Lock()
			philo.philos.deathTime = now - philo.philos.startTime
			philo.philos.diedPhilo = philo.id
			philo.philos.mtx.Unlock()
			stop.Store(true)
		}
		return true
	}
	return false
}

// prints each philosopher's status
func printMsg(color string, id uint32, time int64, msg string, reset string) {
	if !stop.Load() {
		fmt.Printf("%s[%v] %d %s\n%s", color, time, id, msg, reset)
	}
}

func Parser(philos *Philosophers, args []string) error {
	number, err := strconv.Atoi(os.Args[1])
	if err != nil || number > 10000 || number < 1 {
		return errors.New("invalid number of philos")
	}
	philos.size = number

	number, err = strconv.Atoi(os.Args[2])
	if err != nil || number > 10000 || number < 1 {
		return errors.New("invalid time to die")
	}
	philos.timeToDie = int64(number)

	number, err = strconv.Atoi(os.Args[3])
	if err != nil || number > 10000 || number < 1 {
		return errors.New("invalid time to eat")
	}
	philos.timeToEat = int64(number)

	number, err = strconv.Atoi(os.Args[4])
	if err != nil || number > 10000 || number < 1 {
		return errors.New("invalid time to sleep")
	}
	philos.timeToSleep = int64(number)
	return nil
}
