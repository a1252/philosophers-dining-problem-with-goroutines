# philosophers-dining-problem-with-goroutines

## Getting started

Dijkstra's Philosophers dining problem with goroutines and mutexes

## Running the program

```bash
go run Philosophers.go NumberOfPhilosophers TimeToDie TimeToEat TimeToSleep
# Example
go run Philosophers.go 2 410 200 200
```

- NumberOfPhilosophers -> number of philosophers in the simulation
- TimeToEat - Duration.Millis that a philosopher spends eating
- TimeToSleep - Duration.Millis that a philosopher spends sleeping
- TimeToDie -> Time gap that a philosopher must start eating in since the last eating time, to avoid dieing, and thereby keeping the simulation going
